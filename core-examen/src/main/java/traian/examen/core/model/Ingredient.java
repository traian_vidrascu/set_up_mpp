package traian.examen.core.model;

import lombok.*;

import javax.persistence.*;

/**
 * Created by traian on 19.06.2017.
 */
@Entity
@Table(name = "ingredient")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Ingredient extends BaseEntity<Long> {

    @Column(name = "name",nullable = false)
    private String name;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    Pizza pizza;
}
