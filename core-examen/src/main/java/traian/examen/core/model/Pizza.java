package traian.examen.core.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by traian on 18.06.2017.
 */
@Entity
@Table(name = "pizza")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Pizza extends BaseEntity<Long> {

    @Column(nullable = false,unique = true)
    String name;

    @Column(nullable = false)
    String description;

    @Column(nullable = false)
    Float price;

    @OneToMany(mappedBy = "pizza", cascade = CascadeType.ALL)
    Set<Ingredient> ingredients = new HashSet<>();

    public void addIngredient(Ingredient ingredient) {

        ingredients.add(ingredient);
    }

    public void addIngredients(Set<Ingredient> ingredients) {
        ingredients.stream()
                .forEach(ingredient -> addIngredient(ingredient));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pizza pizza = (Pizza) o;

        return name.equals(pizza.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }


    /*@Column(nullable = false)
    @Enumerated(EnumType.STRING)
    Cusine cusine;*/

    /*@Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", cusine=" + cusine +
                '}' + super.toString();
    }*/
}
