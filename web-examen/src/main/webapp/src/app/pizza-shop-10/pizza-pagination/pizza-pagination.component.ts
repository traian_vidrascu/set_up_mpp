import { Component, OnInit } from '@angular/core';
import {PizzaService} from "../shared/pizza.service";
import {Pizza} from "../shared/pizza.model";
import {max} from "rxjs/operator/max";
import {Router} from "@angular/router";

@Component({
  selector: 'app-pizza-pagination',
  templateUrl: './pizza-pagination.component.html',
  styleUrls: ['./pizza-pagination.component.css']
})
export class PizzaPaginationComponent implements OnInit {

  page: number = 0;
  selectedPizza: Pizza;
  pizzas: Pizza[];

  constructor(private pizzaService: PizzaService,
              private router: Router) { }

  ngOnInit() {
    this.pizzaService.getPage(this.page).subscribe(
      pizzas => this.pizzas = pizzas
    )
  }

  back(){
    this.page = this.page-1;
    if(this.page < 0){
      this.page = 0;
    }
    this.pizzaService.getPage(this.page).subscribe(
      pizzas => this.pizzas = pizzas
    )

  }
  onSelect(pizza: Pizza): void {
    this.selectedPizza = pizza;
  }
  next(){
    if(this.pizzas.length>0){
      this.page = this.page + 1;
    }
    this.pizzaService.getPage(this.page).subscribe(
      pizzas => this.pizzas = pizzas
    )
  }

  gotoDetail(){
    this.router.navigate(['pizzashop10/detail', this.selectedPizza.id]);
  }

}
