import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaShop10NewComponent } from './pizza-shop-10-new.component';

describe('PizzaShop10NewComponent', () => {
  let component: PizzaShop10NewComponent;
  let fixture: ComponentFixture<PizzaShop10NewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PizzaShop10NewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaShop10NewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
