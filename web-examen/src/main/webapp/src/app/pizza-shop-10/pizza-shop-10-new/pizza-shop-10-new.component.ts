import { Component, OnInit } from '@angular/core';
import {Pizza} from "../shared/pizza.model";
import {PizzaService} from "../shared/pizza.service";

@Component({
  selector: 'app-pizza-shop-10-new',
  templateUrl: './pizza-shop-10-new.component.html',
  styleUrls: ['./pizza-shop-10-new.component.css']
})
export class PizzaShop10NewComponent implements OnInit {

  pizza: Pizza;
  cusines = ['MEDITERRANEAN','ORIENTAL'];

  constructor(private pizzaService: PizzaService) { }

  ngOnInit() {
    this.pizza = new Pizza();
  }

  save(){
    console.log(this.pizza)
    this.pizzaService.create(this.pizza.name,this.pizza.description,this.pizza.price).subscribe(pizza => alert("pizza added!"));
  }

}
