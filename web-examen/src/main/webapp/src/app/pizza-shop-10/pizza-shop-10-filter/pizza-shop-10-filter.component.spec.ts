import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaShop10FilterComponent } from './pizza-shop-10-filter.component';

describe('PizzaShop10FilterComponent', () => {
  let component: PizzaShop10FilterComponent;
  let fixture: ComponentFixture<PizzaShop10FilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PizzaShop10FilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaShop10FilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
