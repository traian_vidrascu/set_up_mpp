package traian.examen.web.converter;

import org.springframework.stereotype.Component;
import traian.examen.core.model.Ingredient;

import traian.examen.web.dto.IngredientDto;

/**
 * Created by traian on 19.06.2017.
 */
@Component
public class IngredientConverter extends BaseConverter<Ingredient,IngredientDto> {

    @Override
    public Ingredient convertDtoToModel(IngredientDto ingredientDto){

        Ingredient ingredient = Ingredient.builder()
                .name(ingredientDto.getName())
                .build();

        ingredient.setId(ingredientDto.getId());

        return ingredient;
    }

    @Override
    public IngredientDto convertModelToDto(Ingredient ingredient){

        IngredientDto ingredientDto = IngredientDto.builder()
                .name(ingredient.getName())
                .build();

        ingredientDto.setId(ingredient.getId());

        return ingredientDto;
    }
}
