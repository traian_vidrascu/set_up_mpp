package traian.examen.web.converter;

/**
 * Created by traian on 16.06.2017.
 */
public interface ConverterGeneric<Model, Dto> {
    Model convertDtoToModel(Dto dto);

    Dto convertModelToDto(Model model);
}
