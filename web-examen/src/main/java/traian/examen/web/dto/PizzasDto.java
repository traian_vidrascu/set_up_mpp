package traian.examen.web.dto;

import lombok.*;

import java.util.List;

/**
 * Created by traian on 18.06.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
public class PizzasDto {

    List<PizzaDto> pizzas;
}
