package traian.examen.web.dto;

import lombok.*;
import traian.examen.core.model.Cusine;

/**
 * Created by traian on 18.06.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class PizzaDto extends BaseDto {

    private String name;

    private String description;

    private Float price;

}
