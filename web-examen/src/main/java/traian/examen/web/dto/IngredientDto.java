package traian.examen.web.dto;

import lombok.*;

/**
 * Created by traian on 19.06.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class IngredientDto extends BaseDto{

    private String name;
}
