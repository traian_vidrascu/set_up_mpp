package traian.examen.web.dto;

import lombok.*;

/**
 * Created by traian on 18.06.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class FilterDto {

    private String filterType;

    private String filterValue;
}
